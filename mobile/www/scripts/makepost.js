﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        $("#aLogin").click(function () {
            window.location = "login.html";
            return false;
        });

        $("#aManagement").click(function () {
            window.location = "management.html";
            return false;
        });

        $("#aBoard").click(function () {
            window.location = "board.html";
            return false;
        });

        $("#f1").submit(function () {
            var uid = window.localStorage.getItem("uid");
            var ad = { "title": $("#tb1").val(), "msg": $("#tb2").val(), "uid": uid };

            $.ajax({
                url: "http://169.254.80.80:56395/api/Homecare/PPost",
                method: "post",
                data: JSON.stringify(ad),
                contentType: "application/json",
                datatype: "json",
            })
            .done(function (data) {
                alert(data);
            })
            .error(function (data) {
                alert("error");
            });
            return false;
        });

        return false;
    };


    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

} )();