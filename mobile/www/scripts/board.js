﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {

        $("#aLogin").click(function () {
            window.location = "login.html";
            return false;
        });

        $("#aManagement").click(function () {
            window.location = "management.html";
            return false;
        });

        $("#aBoard").click(function () {
            window.location = "board.html";
            return false;
        });

        $("#aMakepost").click(function () {
            window.location = "makepost.html";
            return false;
        });

        $("body").on("click", "a.post", function () {
            var s = $(this).attr('id');
            var aid = parseInt(s);
            window.localStorage.setItem("pid", aid);
            window.location = "viewpost.html";
            return false;
        });

        $("body").on("click", "a.author", function () {
            var s = $(this).attr('id');
            var uid = parseInt(s);
            window.localStorage.setItem("puid", uid);
            window.location = "viewuser.html";
            return false;
        });


        function getBoard(search) {
            $("a").remove(".post");
            $("br").remove(".post");
            $("label").remove(".post");
            $("label").remove(".author");
            search.keyword = $("#tb1").val();

            $.ajax({
                url: "http://169.254.80.80:56395/api/Homecare/GBoard",
                method: "post",
                data: JSON.stringify(search),
                contentType: "application/json",
                datatype: "json",
            })
            .done(function (data) {
                var dataJSON = JSON.parse(data);
                var i = 0;

                for (i in dataJSON) {
                    var s1 = " <br class=\"post\"/><a href=\"\"" + " class=\"post\" id=" + dataJSON[i].aid + "\"a\">" + dataJSON[i].title + "</a>";
                    var s2 = " " + " <a href=\"\"" + " class=\"author\" id=" + dataJSON[i].uid + "\"u\">" + dataJSON[i].author + "</a>";
                    var s3 = " " + "<label class=\"post\">" + dataJSON[i].date + "</label>";

                    $(".board").append(s1);
                    $(".board").append(s2);
                    $(".board").append(s3);
                }
            })
            .error(function (data) {
                alert("error");
            });
            return false;
        };

        $(document).ready(function () {
            var s = { "keyword": "", "page": 0 };
            getBoard(s);
            return false;
        });

        $("#bt1").click(function () {
            var s = { "keyword": "", "page": 0 };
            s.keyword = $("#tb1").val();
            getBoard(s);
            return false;
        });
        
        return false;
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

} )();