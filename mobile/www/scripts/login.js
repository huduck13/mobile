﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        $("#aLogin").click(function () {
            window.location = "login.html";
            return false;
        });

        $("#aManagement").click(function () {
            window.location = "management.html";
            return false;
        });

        $("#aBoard").click(function () {
            window.location = "board.html";
            return false;
        });

        $("#f1").submit(function () {
            var luser = { "id": 0, "un": $("#tb1").val(), "pw": $("#tb2").val() };

            $.ajax({
                url: "http://169.254.80.80:56395/api/Homecare/Login",
                method: "post",
                data: JSON.stringify(luser),
                contentType: "application/json",
                datatype: "json",
            })
            .done(function (data) {
                if (data != 0) {
                    alert("Login successful");
                    window.localStorage.setItem("uid", data);
                    //window.location.replace("/Board");
                }
                else {
                    alert("Login failed");
                }
            })
            .error(function (data) {
                alert("error");
            });
            return false;
        });

        $("#f2").submit(function () {
            var run = $("#tb3").val();
            var rpw = $("#tb4").val();
            var ruser = { "un": run, "pw": rpw }

            if (run.length < 5) {
                alert("username too short!");
            }
            else if (!containsLetter(run) & !containsNumber(run)) {
                alert("username must contain only letters and numbers!");
            }
            else {
                if (rpw.length < 8) {
                    alert("password too short!")
                }
                else if (!(containsLetter(rpw) & containsNumber(rpw))) {
                    alert("password must contain both letters and numbers");
                }
                else {
                    $.ajax({
                        url: "http://169.254.80.80:56395/api/Homecare/Register",
                        method: "post",
                        data: JSON.stringify(ruser),
                        contentType: "application/json",
                        datatype: "json",
                    })
                    .done(function (data) {
                        alert(data);
                    })
                    .error(function (data) {
                        alert("error");
                    });
                }
            }
            return false;
        });
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

    function containsLetter(str) {
        return (str.match(/[a-z]/i) != null);
    };

    function containsNumber(str) {
        return (str.match(/[0-9]/i) != null);
    };

} )();