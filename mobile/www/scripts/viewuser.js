﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        $("#aLogin").click(function () {
            window.location = "login.html";
            return false;
        });

        $("#aManagement").click(function () {
            window.location = "management.html";
            return false;
        });

        $("#aBoard").click(function () {
            window.location = "board.html";
            return false;
        });

        $("#f1").submit(function () {
            var uid = window.localStorage.getItem("puid");
            var ruid = window.localStorage.getItem("uid")
            var rating = { "rating": $("#sl1").val(), "comment": $("#tb1").val(), "uid": uid, "ruid": ruid };

            $.ajax({
                url: "http://169.254.80.80:56395/api/Homecare/RateUser",
                method: "post",
                data: JSON.stringify(rating),
                contentType: "application/json",
                datatype: "json",
            })
            .done(function (data) {
                alert(data);
            })
            .error(function (data) {
                alert("error");
            });

            return false;
        });

        function getRatings() {
            var uid = window.localStorage.getItem("puid");
            var comment = { "uid": uid, "page": 0 };

            $.ajax({
                url: "http://169.254.80.80:56395/api/Homecare/GRatings",
                method: "post",
                data: JSON.stringify(comment),
                contentType: "application/json",
                datatype: "json",
            })
            .done(function (data) {
                var dataJSON = JSON.parse(data);
                var i = 0;

                for (i in dataJSON) {
                    $(".ratings").append("<br />" + dataJSON[i].rating + " " + dataJSON[i].comment);
                }
            })
            .error(function (data) {
                alert("error");
            });
            return false;
        };

        $(document).ready(function () {
            getRatings();
            return false;
        });

        return false;
};


    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

} )();