﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        $("#aLogin").click(function () {
            window.location = "login.html";
            return false;
        });

        $("#aManagement").click(function () {
            window.location = "management.html";
            return false;
        });

        $("#aBoard").click(function () {
            window.location = "board.html";
            return false;
        });

        $(document).ready(function () {
            var pid = window.localStorage.getItem("pid");
            var aid = { "aid": pid };
            
            $.ajax({
                url: "http://169.254.80.80:56395/api/Homecare/GPost",
                method: "post",
                data: JSON.stringify(aid),
                contentType: "application/json",
                datatype: "json",
            })
            .done(function (data) {
                var dataJSON = JSON.parse(data);

                document.getElementById("lb1").innerHTML = dataJSON.title;
                document.getElementById("lb2").innerHTML = dataJSON.msg;

            })
            .error(function (data) {
                alert("error");
            });

            return false;
        });

        return false;
    };


    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

} )();