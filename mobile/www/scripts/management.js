﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        $("#aLogin").click(function () {
            window.location = "login.html";
            return false;
        });

        $("#aManagement").click(function () {
            window.location = "management.html";
            return false;
        });

        $("#aBoard").click(function () {
            window.location = "board.html";
            return false;
        });

        $("#f1").submit(function () {

            var uid = window.localStorage.getItem("uid");

            var uprofile = {
                "id": uid,
                "fn": $("#tb1").val(),
                "ln": $("#tb2").val(),
                "email": $("#tb3").val(),
                "phone": $("#tb4").val(),
                "por": $("#sl1").val(),
                "wp": $("#sl2").val(),
                "en": $("#sl3").val()
            };

            $.ajax({
                url: "http://169.254.80.80:56395/api/Homecare/UpdateProfile",
                method: "post",
                data: JSON.stringify(uprofile),
                contentType: "application/json",
                datatype: "json",
            })
            .done(function (data) {
                alert(data);
            })
            .error(function (data) {
                alert("error");
            });

            return false;
        });
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

} )();